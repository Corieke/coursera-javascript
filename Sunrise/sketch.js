var groundHeight;
var mountain1;
var mountain2;
var mountain3;

var tree;

var moon;
var sun;
var darkness;


function setup()
{
	createCanvas(800, 600);
	//set the groundHeight proportional to the canvas size
	groundHeight = (height/3) * 2;

	//initalise the mountain objects with properties to help draw them to the canvas
	mountain1 = {
		x: 400,
		y: groundHeight,
		height: 320,
		width: 230,
        color: 120
        
	};
	mountain2 = {
		x: 530,
		y: groundHeight,
		height: 200,
		width: 130
	};
    
    mountain3 = {
        x: 570,
        y: groundHeight,
        height: 250,
        width: 200
        
    }

	//initalise the tree object
	tree = {
		x: 150,
		y: groundHeight + 20,
		trunkWidth: 40,
		trunkHeight: 150,
		canopyWidth: 120,
		canopyHeight: 100
	};

    //initalise the sun 
	sun = {
		x: 150,
		y: 70,
		diameter: 80,
	};
    
    //TASK: intialise a moon object with an extra property for brightness
    moon = {
        x: 700,
        y: 70,
        diameter: 80,
        brightness: 0,
               
    }
    
   //stars
    stars = {
        x: 0,
        y: 0,
        brightness: 0,
    }


	//set the initial darkness
	darkness = 0;
}



function draw()
{
	//TASK: update the values for the moons brightness, the sun's position and the darkness.
	//You can either map this to the mouse's location (i.e. the futher left the mouse is the more daylight) or you can just change the values gradually over time.

   
    
	//draw the sky
	background(150, 200, 255);
	noStroke();

	//draw the sun
	fill(255, 255, 0);

    sunmovingY = max(sun.y, mouseX);
    ellipse(sun.x, sunmovingY, sun.diameter);
   
    
    //TASK: you'll need to draw the moon too. Make sure you use brightness to adjust the colour
    

	//draw the ground and make it green
	fill(70, 200, 0);
	rect(0, groundHeight, width, height - groundHeight);

	//draw the mountains
	
    fill(mountain1.color);
	triangle(mountain1.x, mountain1.y,
		mountain1.x + mountain1.width, mountain1.y,
		mountain1.x + (mountain1.width / 2), mountain1.y - mountain1.height);

    fill(mountain1.color + 5);
	triangle(mountain2.x, mountain2.y,
		mountain2.x + mountain2.width, mountain2.y,
		mountain2.x + (mountain2.width / 2), mountain2.y - mountain2.height);
    
    fill(mountain1.color + 10);
    triangle(mountain3.x, mountain3.y,
        mountain3.x + mountain3.width, mountain3.y,
		mountain3.x + (mountain3.width / 2), mountain3.y - mountain3.height);
    
    //TASK: You can draw the tree yourself
    
    fill(115, 64, 12);
    rect(tree.x,
         tree.y - tree.trunkHeight,
         tree.trunkWidth,
         tree.trunkHeight);
    fill(70, 200, 0);
    ellipse(
        tree.x + (tree.trunkWidth / 2),
        tree.y - (tree.trunkHeight/8 * 7),
        tree.canopyWidth,
        tree.canopyHeight);
    
    //darkness
    darkness = (0 + mouseX) / (800/230);
    fill(0, 0, 0, darkness);
    rect(0, 0, width, height);
    
    
    //stars
    stars.brightness = (0 + mouseX) / (800/255);
    fill(255, 255, 235, stars.brightness);
    starsrandomx = random(0, 800);
    starsrandomy = random(0, height - groundHeight);
    ellipse(starsrandomx, starsrandomy, 2, 2);
    
    
    //moon
    moon.brightness = (0 + mouseX) / (800/200);
    fill(255,255,235, moon.brightness);
    ellipse(moon.x, moon.y, moon.diameter);
    

	//TASK: make the scene dark by drawing a rectangle that covers the whole canvas.
	//Use the alpha value of fill to determine how dark to make it



}