function setup()
{
	createCanvas(800, 800);
    background(250, 250, 250);
}

function draw()
{
    
    noStroke();
    
    //circles
    
    //big yellow
    fill (255, 200, 100, 80);
    ellipse(220, 450, 450);
    
    //small light blue
    fill(100, 200, 255, 100);
    ellipse(400, 300, 150);
    
   //medium mint
    fill(221, 243, 213);
    ellipse(600, 630, 200);
    
    //small light mint
    fill(246, 255, 242);
    ellipse (620, 600, 80);
   
    //big green
    fill(152, 178, 142, 40);
    ellipse(600, 700, 400);
    
    
    //small blue
    fill(100, 100, 255, 50);
    ellipse(100, 160, 80);
    
    //small dark purple
    fill(100, 100, 200, 50);
    ellipse(160, 120, 100);
    
    //small pink
    fill(200, 100, 200, 50);
    ellipse(230, 80, 80);
    
    //medium green
    fill(150, 200, 50, 50);
    ellipse(650, 150, 200);
    
    //small purple
    fill(150, 140, 200, 20);
    ellipse(600, 70, 100);
    
    //triangles
    //turquoise big
    fill(0, 200, 200, 10);
    triangle (200, 700, 550, 250, 700, 700);
      
    //small lavender
    fill(213, 211, 249, 50);
    triangle (30, 100, 380, 40, 430, 180);
    
    //rectangles
    fill(200, 100, 100, 20);
    rect(710, 0, 50, 600);
    
    
    //lines
    strokeWeight(2);
    stroke(100);
    line(200, 780, 200, 200);
    line (220, 600, 300, 40);
    
    line(50, 500, 780, 750);
    line(100, 150, 700, 150);
    line(50, 700, 600, 100);
    
    line(400, 50, 750, 400);
}