var SunPos_x;
var SunPos_x;
var RayDeviation_Hor_X_Vert_Y;
var RayDeviation_Hor_Y_Vert_X;
var RayDistancePointC;



function setup() 
{
    createCanvas(512,512);
    SunPos_x = 380
    SunPos_y = 130
    
    RayDeviation_Hor_X_Vert_Y = 60
    RayDeviation_Hor_Y_Vert_X = 16
    RayDistancePointC = 130
    
    DiagRayDeviation_A = 54.23
    DiagRayDeviation_B = 35.36
    DiagRayPointC = 92
    
    
    
}

function draw()
{
    background(150,150,255);
    
    //sun
    noStroke();
    fill(255,150,0);
    ellipse(SunPos_x,SunPos_y,100,100);
    
    fill(255, 150, 0, 30);
    ellipse(SunPos_x, SunPos_y, 390, 390);
    
    //sunrays
    // right
    fill(255, 150, 0);
    triangle(SunPos_x + RayDeviation_Hor_X_Vert_Y, SunPos_y - RayDeviation_Hor_Y_Vert_X, 
            SunPos_x + RayDeviation_Hor_X_Vert_Y, SunPos_y + RayDeviation_Hor_Y_Vert_X,
            SunPos_x + RayDistancePointC, SunPos_y);
    
    // left
    triangle(SunPos_x - RayDeviation_Hor_X_Vert_Y, SunPos_y - RayDeviation_Hor_Y_Vert_X, 
            SunPos_x - RayDeviation_Hor_X_Vert_Y, SunPos_y + RayDeviation_Hor_Y_Vert_X,
            SunPos_x - RayDistancePointC, SunPos_y);
    
    
    // above
    
    triangle(SunPos_x - RayDeviation_Hor_Y_Vert_X, SunPos_y - RayDeviation_Hor_X_Vert_Y, 
            SunPos_x + RayDeviation_Hor_Y_Vert_X, SunPos_y - RayDeviation_Hor_X_Vert_Y,
            SunPos_x, SunPos_y - RayDistancePointC);
    
    // beneath
    
    triangle(SunPos_x - RayDeviation_Hor_Y_Vert_X, SunPos_y + RayDeviation_Hor_X_Vert_Y, 
            SunPos_x + RayDeviation_Hor_Y_Vert_X, SunPos_y + RayDeviation_Hor_X_Vert_Y,
            SunPos_x, SunPos_y + RayDistancePointC);
    
   
    // upper left
    triangle (SunPos_x - DiagRayDeviation_A, SunPos_y - DiagRayDeviation_B,
              SunPos_x - DiagRayDeviation_B, SunPos_y - DiagRayDeviation_A,
              SunPos_x - DiagRayPointC, SunPos_y - DiagRayPointC
            );
    
    
    //upper right
    triangle (SunPos_x + DiagRayDeviation_A, SunPos_y - DiagRayDeviation_B,
              SunPos_x + DiagRayDeviation_B, SunPos_y - DiagRayDeviation_A,
              SunPos_x + DiagRayPointC, SunPos_y - DiagRayPointC
            );
    
    
    //lower left
    triangle (SunPos_x - DiagRayDeviation_A, SunPos_y + DiagRayDeviation_B,
              SunPos_x - DiagRayDeviation_B, SunPos_y + DiagRayDeviation_A,
              SunPos_x - DiagRayPointC, SunPos_y + DiagRayPointC
            );
    
    
    // lower right
    
    triangle (SunPos_x + DiagRayDeviation_A, SunPos_y + DiagRayDeviation_B,
              SunPos_x + DiagRayDeviation_B, SunPos_y + DiagRayDeviation_A,
              SunPos_x + DiagRayPointC, SunPos_y + DiagRayPointC
            );
    
  
    
    
    //tree
    stroke(0);
    fill(180,80,0);
    ellipse(256,360,40,100);
    fill(0,150,0);
    ellipse(256,300,120,120);
    
    //cloud
    noStroke();
    fill(255);
    ellipse(100,50,50,50);
    ellipse(130,50,30,30);
    ellipse(150,50,20,20);
    
    //ground
    fill(200,130,0);
    rect(0,400,width,112);
    

}


