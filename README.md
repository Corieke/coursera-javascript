# Project information
This project contains Javascript exercises made for the Coursera course [Introduction to computer programming](https://www.coursera.org/learn/introduction-to-computer-programming). All code was made using the P5.js library.

## Project overview
- **Create your own variables**
A display of a tree, a cloud and radiant sunshine.

- **Kandinsky**
After the work of painter [Wassily Kandisky](https://en.wikipedia.org/wiki/Wassily_Kandinsky), artwork composed of lines and shapes.

- **Robot parade**
Creating three different robots, one of them is greeting you in his bee suit.

- **Sunrise**
A transition from a sunny day to a starry night, using the mouseY position for the transition.
